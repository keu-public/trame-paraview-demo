# Trame ParaView Demo

First, run a ParaView (distributed) server
```bash
mpirun -np 4 pvserver
```

Then, run the [Trame](https://kitware.github.io/trame/) app that connects to it:
```bash
python -m venv venv
pip install trame trame-vuetify trame-vtk
pvpython --venv $PWD/venv main.py
```

![Distributed ParaView Trame application](trame.jpg)
