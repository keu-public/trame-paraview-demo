import paraview.web.venv

from trame.app import get_server
from trame.widgets import vuetify, paraview
from trame.ui.vuetify import SinglePageLayout

from paraview import simple

server = get_server()
server.client_type = "vue2"
state, ctrl = server.state, server.controller

DEFAULT_RESOLUTION = 6

# Connect to local distributed pvserver host
simple.Connect("localhost",11111)

# Create a distributed sphere
sphere1 = simple.Sphere()
redistributeDataSet1 = simple.RedistributeDataSet(registrationName='RedistributeDataSet1', Input=sphere1)
representation = simple.Show(redistributeDataSet1)

# Color it by process IDs
simple.ColorBy(representation, ('CELLS', 'vtkProcessId'))
vtkProcessIdLUT = simple.GetColorTransferFunction('vtkProcessId')
vtkProcessIdLUT.TransferFunction2D = simple.GetTransferFunction2D('vtkProcessId')
vtkProcessIdLUT.RGBPoints = [0.0, 0.0, 0.0, 0.0, 1.2000000000000002, 0.901960784314, 0.0, 0.0, 2.4000000000000004, 0.901960784314, 0.901960784314, 0.0, 3.0, 1.0, 1.0, 1.0]
vtkProcessIdLUT.ColorSpace = 'RGB'
vtkProcessIdLUT.NanColor = [0.0, 0.498039215686, 1.0]
vtkProcessIdLUT.ScalarRangeInitialized = 1.0
representation.LookupTable = vtkProcessIdLUT

view = simple.Render()


@state.change("resolution")
def update_sphere(resolution, **kwargs):
    sphere1.ThetaResolution = resolution
    ctrl.view_update()


def update_reset_resolution():
    state.resolution = DEFAULT_RESOLUTION

state.trame__title = "ParaView sphere"

with SinglePageLayout(server) as layout:
    layout.icon.click = ctrl.view_reset_camera
    layout.title.set_text("Distributed Sphere Application")

    with layout.toolbar:
        vuetify.VSpacer()
        vuetify.VSlider(
            v_model=("resolution", DEFAULT_RESOLUTION),
            min=3,
            max=100,
            step=1,
            hide_details=True,
            dense=True,
            style="max-width: 300px",
        )
        vuetify.VDivider(vertical=True, classes="mx-2")
        with vuetify.VBtn(icon=True, click=update_reset_resolution):
            vuetify.VIcon("mdi-undo-variant")

    with layout.content:
        with vuetify.VContainer(
            fluid=True,
            classes="pa-0 fill-height",
        ):
            html_view = paraview.VtkRemoteView(view)
            ctrl.view_update = html_view.update
            ctrl.view_reset_camera = html_view.reset_camera

if __name__ == "__main__":
    server.start()
